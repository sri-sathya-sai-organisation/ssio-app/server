import {SignInService} from "../../api/auth/SignInService";
import {SignInProvider, SignInRequest} from "../../api/auth/SignInRequest";
import {SignInResponse} from "../../api/auth/SignInResponse";
import * as firebase from "firebase-admin";
import {inject} from "inversify";
import {Cache} from "../../api/state/Cache";
import {sign, verify, VerifyErrors} from "jsonwebtoken";
import {Config} from "node-config-ts";
import {SignedInUser} from "../../api/auth/SignedInUser";
import {PersistentStore} from "../../api/state/PersistentStore";
import {User} from "../../api/model/User";
import {OnboardingStage} from "../../api/model/OnboardingStage";
import {APIException} from "../../api/APIException";
import DecodedIdToken = firebase.auth.DecodedIdToken;


export class SignInServiceImpl extends SignInService {
    constructor(@inject(Cache.name) private cache: Cache, @inject(PersistentStore.name) private store: PersistentStore, @inject("config") private config: Config) {
        super();
        firebase.initializeApp();
    }

    signIn(req: SignInRequest): Promise<SignInResponse> {
        if (req.provider === SignInProvider.FIREBASE) {
            // idToken comes from the client app
            return firebase
                .auth()
                .verifyIdToken(req.authenticationToken)
                .then((decodedToken: DecodedIdToken) => {
                    const email = decodedToken.email;
                    return this.store.findUser(email)
                        .then(user => {
                            if (user) {
                                return user;
                            }
                            const newUser = new User();
                            newUser.id = email;
                            newUser.stage = OnboardingStage.SIGNED_UP;
                            return this.store.saveUser(
                                newUser
                            );
                        }).then(user => {
                            const signedInUser = {
                                id: user.id
                            };
                            const jwt = sign(signedInUser, this.config.secrets.jwtSigningKey, {
                                expiresIn: this.config.auth.jwtExpiryTime,
                                audience: "sathyasaiyaapp"
                            });
                            return {
                                jwt
                            };
                        });
                });
        }
        return Promise.reject(`Unsupported provider in req ${req}`);
    }

    validate(jwt: string): Promise<User> {
        return new Promise((resolve, reject) => {
            verify(jwt, Config.secrets.jwtSigningKey, {
                audience: "sathyasaiyaapp",
                ignoreExpiration: false
            }, (err: VerifyErrors | null, o: object) => {
                if (err) {
                    reject(err);
                } else {
                    const siu = o as SignedInUser;
                    this.store.findUser(siu.id)
                        .then(user => {
                            if (user) {
                                resolve(user);
                            } else {
                                throw new APIException("API_UNAUTHORIZED_USER");
                            }
                        });
                }
            });
        });
    }

}
