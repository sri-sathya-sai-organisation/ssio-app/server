import {Cache, KeyValuePair} from "../../api/state/Cache";
import * as redis from "redis";
import {promisify} from "util";
import {injectable} from "inversify";
import {ClientOpts} from "redis";
import {config} from "node-config-ts";

export class CacheImpl extends Cache {
    private client = redis.createClient({host: config.redis.host});
    private cacheGet: (key: string) => Promise<string> = null;
    private cacheSet: (key: string, value: string) => Promise<string> = null;

    constructor() {
        super();
        this.cacheGet = promisify(this.client.get);
        this.cacheSet = promisify(this.client.set);
    }

    get<T>(key: string): Promise<T> {
        return this.cacheGet(key).then(value => JSON.parse(key) as T);
    }

    set<T>(key: string, value: T): Promise<KeyValuePair<T>> {
        return this
            .cacheSet(key, JSON.stringify(value))
            .then(_ => ({
                key,
                value
            }));
    }

}
