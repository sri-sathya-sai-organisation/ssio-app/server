import "reflect-metadata";
import {injectable} from "inversify";
import {PersistentStore} from "../../api/state/PersistentStore";
import {User} from "../../api/model/User";
import {Connection, getConnectionManager} from "typeorm";
import {config} from "node-config-ts";


@injectable()
export class PersistentStoreImpl extends PersistentStore {
    private readonly connection: Connection;

    constructor() {
        super();
        this.connection = getConnectionManager().create({
            type: "postgres",
            url: `postgres://${config.postgres.userName}:${config.secrets.postgresPassword}@${config.postgres.host}/sathyasaiyaapp`,
            entities: [
                User
            ]
        });
    }

    private connect() {
        if (this.connection.isConnected) {
            return Promise.resolve(this.connection);
        } else {
            return this.connection.connect();
        }
    }

    deleteUser(id: string): Promise<User> {
        return Promise.resolve(undefined);
    }

    findUser(id: string): Promise<User | undefined> {
        return this.connect()
            .then(connection => {
                return connection.manager.getRepository(User).findOne(id);
            });
    }

    saveUser(user: User): Promise<User> {
        return this.connect()
            .then(connection => {
                return connection.manager.getRepository(User).save(user);
            });
    }

}
