import {inject, injectable} from "inversify";
import {SignInService} from "../api/auth/SignInService";
import {Config} from "node-config-ts";
import {Request, ResponseToolkit, Server, ServerRoute} from "@hapi/hapi";
import {OnboardingStage} from "../api/model/OnboardingStage";
import {APIServer} from "../api/APIServer";
import * as Boom from "@hapi/boom";
import {isEmpty} from "lodash";
import * as yar from "@hapi/yar";
import {I18nPlugin} from "./i18n/I18nPlugin";
import {Logger} from "../api/Logger";

const Inert = require("@hapi/inert");
const Vision = require("@hapi/vision");
const HapiSwagger = require("hapi-swagger");

type AuthSchemeOptions = {
    usersWhoHaveCompletedProfileOnly: boolean;
}

@injectable()
export class APIServerImpl extends APIServer {
    private readonly server: Server;
    private initialized = false;

    constructor(@inject(SignInService.name) signInService: SignInService, @inject("config") config: Config, @inject(Logger.name) private readonly logger: Logger) {
        super(config);
        this.server = new Server({
            port: config.server.port,
            host: config.server.host,
            routes: {
                cors: {
                    origin: ["*"]
                }
            }
        });

        this.server.auth.scheme("jwt", (server: Server, options?: AuthSchemeOptions) => ({
            authenticate: (request: Request, h: ResponseToolkit) => {
                const authorizationHeader = request.headers["authorization"];
                if (!isEmpty(authorizationHeader)) {
                    const jwt = authorizationHeader.startsWith("Bearer") ? authorizationHeader.substr("Bearer".length).trim() : authorizationHeader;
                    return signInService
                        .validate(jwt)
                        .then(user => {
                            if (options && options.usersWhoHaveCompletedProfileOnly) {
                                if (user.stage >= OnboardingStage.PROFILE_COMPLETED) {
                                    return h.authenticated({
                                        credentials: {
                                            user
                                        }
                                    });
                                } else {
                                    throw Boom.unauthorized(request.i18n("API_INCOMPLETE_PROFILE"));
                                }
                            } else {
                                return h.authenticated({
                                    credentials: {
                                        user
                                    }
                                });
                            }
                        })
                        .catch(e => {
                            throw Boom.unauthorized(request.i18n("API_UNAUTHORIZED_USER"));
                        });
                } else {
                    throw Boom.unauthorized(request.i18n("API_UNAUTHORIZED_USER"));
                }
            }
        }));
        this.server.auth.strategy("incompleteProfile", "jwt", {usersWhoHaveCompletedProfileOnly: false});
        this.server.auth.strategy("profileCompleted", "jwt", {usersWhoHaveCompletedProfileOnly: true});
        this.server.auth.default("profileCompleted");
    }

    private async init() {
        if (this.initialized) {
            return;
        }
        await this.server.register({
            plugin: yar,
            options: {
                storeBlank: false,
                cookieOptions: {
                    password: this.config.secrets.cookieEncryptionKey,
                    isSecure: true
                }
            }
        });

        await this.server.register({
            plugin: I18nPlugin
        });

        const swaggerOptions = {
            info: {
                title: "Sathya Sai App API Documentation",
                version: "0.1",
            },
            securityDefinitions: {
                Bearer: {
                    type: "apiKey",
                    name: "Authorization",
                    in: "header"
                }
            },
            security: [{Bearer: []}],
        };

        await this.server.register([
            Inert,
            Vision,
            {
                plugin: HapiSwagger,
                options: swaggerOptions
            },

        ]);
        this.initialized = true;
    }

    public async start() {
        await this.init();
        await this.server.start();
        this.logger.info(`API Server Instance started on ${this.server.info.uri}`);
        this.server.table().forEach((route) => this.logger.info(`${route.method}  ${route.path}`));
        this.logger.info(`=============API Server Instance started on ${this.server.info.uri}=============`);

    }

    public addRoute(route: ServerRoute) {
        this.server.route(route);
    }
}
