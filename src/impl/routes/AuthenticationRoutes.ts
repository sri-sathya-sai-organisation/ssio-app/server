import {APIServer} from "../../api/APIServer";
import {SignInProvider, SignInRequest} from "../../api/auth/SignInRequest";
import {inject, injectable} from "inversify";
import {SignInService} from "../../api/auth/SignInService";
import {Logger} from "../../api/Logger";
import * as Joi from "@hapi/joi";
import * as Boom from "@hapi/boom";

@injectable()
export class AuthenticationRoutes {
    constructor(@inject(APIServer.name) server: APIServer, @inject(SignInService.name) signInService: SignInService, @inject(Logger.name) logger: Logger) {
        logger.info("Adding authentication routes");
        server.addRoute(
            {
                method: "POST",
                path: "/authentication/signIn",
                options: {
                    tags: ["api"],
                    auth: false,
                    validate: {
                        payload: Joi.object({
                            "authenticationToken": Joi.string().required(),
                            "provider": Joi.string().valid(SignInProvider.FIREBASE)
                        })
                    }
                },
                handler: async (req, h) => {
                    const signInRequest = req.payload as SignInRequest;
                    return signInService
                        .signIn(signInRequest)
                        .then(signInResponse => {
                            return h.response(signInResponse);
                        }).catch(e => {
                            throw Boom.unauthorized(req.i18n("API_UNAUTHORIZED_USER"));
                        });
                }
            }
        );
    }
}
