import {Plugin, Server} from "@hapi/hapi";
import {I18NServiceImpl} from "./I18NServiceImpl";
import {I18NService} from "../../api/i18n/I18NService";


type I18nOptions = {};
const I18nPlugin: Plugin<I18nOptions> = {
    name: "i18n",
    register(server: Server, options: any): void | Promise<void> {
        server.decorate("request", "i18n", function(key: string) {
            if (!this.plugins["i18n"]) {
                this.plugins["i18n"] = new I18NServiceImpl(this.headers["accept-language"]);
            }
            const service = this.plugins["i18n"] as I18NService;
            return service.get(key);
        });
        server.decorate("request", "i18nService", function() {
            if (!this.plugins["i18n"]) {
                this.plugins["i18n"] = new I18NServiceImpl(this.headers["accept-language"]);
            }
            return this.plugins["i18n"] as I18NService;
        });
    }
};

export {I18nPlugin, I18nOptions};
