import {I18NService} from "../../api/i18n/I18NService";
import {injectable} from "inversify";
import {TFunction} from "i18next";
import * as fs from "fs";
import {filter} from "lodash";
import {pick as pickI18nLanguage} from "accept-language-parser";
import * as path from "path";

const i18next = require("i18next");

const NAMESPACE = "api";
const languageToMessageMap = new Map<string, TFunction>();

const promises = filter(fs.readdirSync("messages"), (s) => path.extname(s) === ".json")
    .map(s => {
        const language = path.parse(s).name;
        const filePath = `messages/${language}.json`;
        const messages = JSON.parse(fs.readFileSync(filePath).toString());
        return i18next.createInstance({
            lng: language,
            fallbackLng: "en-UK",
            defaultNS: NAMESPACE,
            fallbackNS: NAMESPACE,
            resources: {
                [language]: {
                    [NAMESPACE]: messages
                }
            }
        }, (err, t) => {
            if (!err) {
                languageToMessageMap.set(language, t);
            }
        });
    });

@injectable()
export class I18NServiceImpl extends I18NService {
    private t: TFunction;

    constructor(public readonly language: string) {
        super();
        const supportedLanguages = Array.from(languageToMessageMap.keys());
        const languageKey = pickI18nLanguage(supportedLanguages, language);
        this.t = languageToMessageMap.get(languageKey);
    }

    public get(key: string): string {
        return this.t(key);
    }
}
