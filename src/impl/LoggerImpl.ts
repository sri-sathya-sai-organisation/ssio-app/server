import * as winston from "winston";
import {Logger} from "../api/Logger";
import {inject, injectable} from "inversify";
import {Config} from "node-config-ts";

@injectable()
export class LoggerImpl extends Logger {
    private readonly logger: winston.Logger;

    constructor(@inject("config")config: Config) {
        super(config);
        this.logger = winston.createLogger({
            level: "info",
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.printf(info => `${info.timestamp} [${info.level}] ${info.message}`),
            ),
            transports: [
                new winston.transports.File({filename: "error.log", level: "error"}),
                new winston.transports.File({filename: "default.log"})
            ]
        });

//
        if (process.env.NODE_ENV !== "production") {
            this.logger.add(new winston.transports.Console({
                format: winston.format.combine(
                    winston.format.timestamp(),
                    winston.format.printf(info => `${info.timestamp} [${info.level}] ${info.message}`),
                )
            }));
        }
    }

    debug(msg: string) {
        this.logger.debug(msg);
    }

    error(msg: string) {
        this.logger.error(msg);
    }

    trace(msg: string) {
        this.logger.silly(msg);
    }

    warn(msg: string) {
        this.logger.warn(msg);
    }

    info(msg: string) {
        this.logger.info(msg);
    }
}

