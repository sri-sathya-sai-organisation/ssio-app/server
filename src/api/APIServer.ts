import {injectable} from "inversify";
import {Config} from "node-config-ts";
import {ServerRoute} from "@hapi/hapi";

@injectable()
export abstract class APIServer {
    protected constructor(protected readonly config: Config) {
    }

    public abstract async start();

    public abstract addRoute(route: ServerRoute);
}
