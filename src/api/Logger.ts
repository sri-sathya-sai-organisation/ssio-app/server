import {Config} from "node-config-ts";
import {injectable} from "inversify";

@injectable()
export abstract class Logger {
    protected constructor(protected readonly config: Config) {
    }

    public abstract error(msg: string);

    public abstract warn(msg: string);

    public abstract info(msg: string);

    public abstract debug(msg: string);

    public abstract trace(msg: string);
}
