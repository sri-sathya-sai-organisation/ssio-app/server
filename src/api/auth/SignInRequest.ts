export enum SignInProvider {
    FIREBASE = "firebase"
}

export class SignInRequest {
    authenticationToken: string;

    provider: SignInProvider;
}
