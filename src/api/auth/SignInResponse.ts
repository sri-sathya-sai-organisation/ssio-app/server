export type SignInResponse = {
    jwt: String
}
