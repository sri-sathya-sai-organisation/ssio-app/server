import {injectable} from "inversify";
import {SignInRequest} from "./SignInRequest";
import {SignInResponse} from "./SignInResponse";
import {SignedInUser} from "./SignedInUser";
import {User} from "../model/User";

@injectable()
export abstract class SignInService {
    abstract signIn(req: SignInRequest): Promise<SignInResponse>

    abstract validate(jwt: string): Promise<User>
}
