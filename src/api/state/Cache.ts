import {injectable} from "inversify";

export type KeyValuePair<T> = {
    key: string;
    value: T;
}

@injectable()
export abstract class Cache {
    abstract get<T>(key: string): Promise<T>

    abstract set<T>(key: string, o: T): Promise<KeyValuePair<T>>
}
