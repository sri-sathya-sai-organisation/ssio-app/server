import {injectable} from "inversify";
import {User} from "../model/User";

@injectable()
export abstract class PersistentStore {
    public abstract findUser(id: String): Promise<User | undefined>

    public abstract saveUser(user: User): Promise<User>

    public abstract deleteUser(id: string): Promise<User>
}
