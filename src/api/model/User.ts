import {Column, Entity, PrimaryColumn} from "typeorm";
import {Gender} from "./Gender";
import {AgeBracket} from "./AgeBracket";
import {OnboardingStage} from "./OnboardingStage";

@Entity({
    "name": "USER",
    "schema": "APP"
})
export class User {

    @PrimaryColumn({
        name: "ID"
    })
    id: string;

    @Column({
        name: "FIRST_NAME"
    })
    firstName: string;

    @Column({
        name: "LAST_NAME"
    })
    lastName: string;

    @Column({
        name: "GENDER",
        enum: Gender
    })
    gender: string;

    @Column({
        name: "AGE_BRACKET",
        enum: AgeBracket
    })
    age: string;

    @Column({
        name: "STAGE",
        enum: OnboardingStage,
        default: OnboardingStage.SIGNED_UP
    })
    stage: number;

}
