export enum AgeBracket {
    a1820 = "18-20",
    a2025 = "20-25",
    a2530 = "25-30",
    a3035 = "30-35"
}
