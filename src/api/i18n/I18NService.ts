import {injectable} from "inversify";

declare module "@hapi/hapi" {
    interface Request {
        i18n: (key: string) => string;
        i18nService: () => I18NService;
    }
}

@injectable()
export abstract class I18NService {
    public abstract get(key: string): string;
}
