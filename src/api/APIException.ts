import {Boom, Options} from "@hapi/boom";

export class APIException extends Boom {
    constructor(public readonly errorCode, options?: Options<any>) {
        super(errorCode, options);
    }
}
