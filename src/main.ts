import {container} from "./ioc";
import {APIServer} from "./api/APIServer";
import {Constants} from "./api/Constants";
import {config} from 'node-config-ts'

const init = async () => {
    container.getAll<any>(Constants.LOAD_EAGERLY);
    console.log (JSON.stringify(config, null, 2));
    const server = container.get<APIServer>(APIServer.name);
    await server.start();
};
process.on("unhandledRejection", (err) => {
    console.log(err);
    process.exit(1);
});

init();
