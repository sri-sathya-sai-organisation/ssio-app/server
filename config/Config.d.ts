/* tslint:disable */
/* eslint-disable */
declare module "node-config-ts" {
  interface IConfig {
    secrets: Secrets
    redis: Redis
    postgres: Postgres
    server: Server
    auth: Auth
  }
  interface Auth {
    jwtExpiryTime: number
  }
  interface Server {
    host: undefined
    port: number
  }
  interface Postgres {
    host: string
    userName: string
  }
  interface Redis {
    host: string
  }
  interface Secrets {
    cookieEncryptionKey: string
    jwtSigningKey: string
    postgresPassword: string
  }
  export const config: Config
  export type Config = IConfig
}
