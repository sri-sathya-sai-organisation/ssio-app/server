#### Run on PC
1. Use `npm run build:config` when you change configuration
1. Use `npm run watch` to develop and run code
1. Use `npm run start` to not restart

#### Run on docker with docker-compose
0. Pre-req: Install [docker](https://docs.docker.com/desktop/) and docker-compose: ```brew install docker-compose```
1. Make sure you are in ```scripts/docker/server``` directory, you use `cd ../scripts/docker/server` from the current directory
1. Run ```docker-compose up```
1. Once you see in your terminal 
```
app_server | 2020-05-05T13:46:45.040Z [info] =============API Server Instance started on http://c84cc971f133:3006=============
```
Open your browser and navigate to http://localhost:3006/documentation

#### Inspecting / Debugging with Chrome Debugger
1. Open the URL [chrome://inspect/#devices](chrome://inspect/#devices)
1. You should see inspect main.js in there if not follow https://nodejs.org/en/docs/guides/debugging-getting-started/ 
