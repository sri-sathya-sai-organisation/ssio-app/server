ALTER TABLE "USER"
    ADD COLUMN "FIRST_NAME"  varchar,
    ADD COLUMN "LAST_NAME"   varchar,
    ADD COLUMN "GENDER"      char(1),
    ADD COLUMN "AGE_BRACKET" char(5);
