const Postgrator = require('postgrator');
const env = process.env || {};

const postgrator = new Postgrator({
    migrationDirectory: __dirname + '/migrations',
    driver: 'pg',
    host: env.POSTGRES_HOST || '127.0.0.1',
    port: env.POSTGRES_PORT || 5432,
    database: env.POSTGRES_DATABASE || "sathyasaiyaapp",
    username: env.POSTGRES_USERNAME || "app",
    password: env.POSTGRES_PASSWORD || null,
    schemaTable: 'schemaversion',
});

postgrator
    .migrate()
    .then((appliedMigrations) => console.log(appliedMigrations))
    .catch((error) => console.log(error));
