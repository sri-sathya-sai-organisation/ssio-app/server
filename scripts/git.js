#!/usr/bin/node

'use strict';

const exec= require('child_process').exec;

const puts = function(error, stdout, stderr){};

const repoUrl = "https://oauth2:{token}@github.com/chavs/sathyasaiyaapp";
const pat = process.env.GITHUB_PAT; // Personal Access Token

/**
 * Performs Git Pull
 */
function gitPull(branch){
    let authRepoUrl = repoUrl.replace('{token}', pat);
    let command = `git pull ${authRepoUrl} ${branch}`;

    exec(command, puts);
}

/**
 * Perform Git Fetch
 */
function gitFetch(branch){
    let authRepoUrl = repoUrl.replace('{token}', pat);
    let command = `git fetch ${authRepoUrl} ${branch}`;

    exec(command, puts);
}

/**
 * Peform Git Clone
 */
function gitClone(branch, path){
    let authRepoUrl = repoUrl.replace('{token}', pat);
    let command = `git clone -b ${branch} ${authRepoUrl} ${path}`;

    exec(command, puts);
}

/**
 * Determine if a git clone, pull or fetch should be performed
 */
function handleCommandArgs(){
    if(process.argv[2] == 'clone'){
        let branch = process.argv[3] ? process.argv[3] : "master"; // master is default branch
        let path = process.argv[4] ? process.argv[4] : "";
        gitClone(branch, path);
    }else if(process.argv[2] == 'pull'){
        let branch = process.argv[3] ? process.argv[3] : "";
        gitPull(branch);
    }else if(process.argv[2] == 'fetch'){
        let branch = process.argv[3] ? process.argv[3] : "";
        gitFetch(branch);
    }
}
handleCommandArgs();