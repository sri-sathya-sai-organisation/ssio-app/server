#!/bin/sh

cd /var/www/server

npm install
npm run build

if [[ "$NODE_ENV" = "local" ]]
then
    node ./db/postgrator.js
    npm run watch
elif [[ "$NODE_ENV" = "testing" ]]
then
    node postgrator.js
    fetch-repo.sh
    npm run test
else
    fetch-repo.sh
    tail -f /var/log/nginx/server.access.log
fi
