#!/usr/bin/env bash

# Clone directory if it does not exist. Else if directory exists and it's not a
# git directory(i.e .git file does not exist), then delete the existing directory
# and re-clone it. Else run git pull on existing directory
if [[ ! -d /var/www/server ]]
then
    #clone the repository. We use the branch environment variable set in docker-compose
    git.js clone $BRANCH /var/www/sathya
elif [[ -d /var/www/server ]] && [[ ! -d /var/www/sathya/.git ]]
then
    #delete and reclone the directory
    rm -rf /var/www/sathya && cd /var/www/ && git.js clone $BRANCH sathya
else
    cd /var/www/sathya && git.js pull $BRANCH
fi

# check that git clone was successful or exit with error
if [[ ! -d /var/www/sathya ]]
then
    echo "Git clone failed! Directory server does not exist"
    exit
fi