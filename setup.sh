set -o xtrace
brew update
brew install postgresql
brew services start postgresql
#psql -U postgres \create user app;
#psql -U postgres \create role app;
psql -U postgres -f db/init.sql
brew install redis
brew install node
npm install
node postgrator.js
